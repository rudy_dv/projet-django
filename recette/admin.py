from django.contrib import admin
from recette.models import Recette, Note, Commentaire


class RecetteAdmin(admin.ModelAdmin):
    list_display = ['id', 'titre']
    list_display_links = ['titre']

class NoteAdmin(admin.ModelAdmin):
    list_display = ['id', 'note', 'utilisateur', 'recette']
    list_display_links = ['note']


class CommentaireAdmin(admin.ModelAdmin):
    list_display = ['id', 'commentaire', 'utilisateur', 'recette']
    list_display_links = ['commentaire']


admin.site.register(Recette, RecetteAdmin)
admin.site.register(Note, NoteAdmin)
admin.site.register(Commentaire, CommentaireAdmin)