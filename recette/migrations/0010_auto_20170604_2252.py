# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-04 22:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recette', '0009_commentaire'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commentaire',
            name='date',
            field=models.DateTimeField(),
        ),
    ]
