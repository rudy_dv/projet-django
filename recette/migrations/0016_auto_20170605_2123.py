# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-05 19:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recette', '0015_auto_20170605_2030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recette',
            name='photo',
            field=models.ImageField(default='static/images/recette/defaut.jpg', upload_to='static/images/recette/'),
        ),
    ]
