# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-05 16:00
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recette', '0010_auto_20170604_2252'),
    ]

    operations = [
        migrations.AddField(
            model_name='recette',
            name='utilisateur',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='recette_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
