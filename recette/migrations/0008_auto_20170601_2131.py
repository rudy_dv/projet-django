# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-01 21:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recette', '0007_note'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='note',
            field=models.SmallIntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5)], default=5),
        ),
    ]
