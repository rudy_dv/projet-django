from random import randint

from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from datetime import datetime

from carnet.models import Fiche
from recette.forms import RecetteForm, NoteForm, CommentaireForm, TriRechercheForm
from recette.models import Recette, Note, Commentaire


# Constantes
NB_COMMENTAIRES_PAR_PAGE = 2
NB_RECETTES_PAR_PAGE = 3


def index(request):
    recettes = Recette.objects.all()

    recettes = paginer(request.GET.get('page'), recettes, NB_RECETTES_PAR_PAGE)

    return render(request, 'liste.html', {
        'recettes': recettes
    })


def voir(request, slug):
    recette = get_object_or_404(Recette, slug=slug)
    all_commentaires = Commentaire.objects.filter(recette=recette)
    a_vote = False
    a_ajouter_au_carnet = False

    commentaires = paginer(request.GET.get('page'), all_commentaires, NB_COMMENTAIRES_PAR_PAGE)

    # Formulaires
    formulaire_note = NoteForm()
    formulaire_commentaire = CommentaireForm()

    # Savoir si l'utilisateur a déjà voté ou non
    if request.user.is_authenticated and Note.objects.filter(recette=recette, utilisateur=request.user).exists():
        a_vote = True

    # Si l'utilisateur a déjà ajouté cette recette à son carnet ou non
    if request.user.is_authenticated and Fiche.objects.filter(utilisateur=request.user, recette=recette).exists():
        a_ajouter_au_carnet = True

    return render(request, 'voir.html', {
        'recette': recette,
        'commentaires': commentaires,
        'formulaire_note': formulaire_note,
        'formulaire_commentaire': formulaire_commentaire,
        'a_vote': a_vote,
        'a_ajouter_au_carnet': a_ajouter_au_carnet
    })


@login_required(login_url='utilisateurs:connexion')
def ajouter(request):
    erreur = ''
    if request.method == "POST":
        formulaire = RecetteForm(request.POST, request.FILES)
        if formulaire.is_valid():
            post = request.POST
            files = request.FILES

            if 'photo_recette' in files:
                photo = files['photo_recette']
            else:
                photo = ''

            if recette_est_correcte(post, photo):
                if not Recette.objects.filter(titre=post['titre']).exists():
                    recette = Recette(
                        titre=post['titre'],
                        type=post['type'],
                        difficulte=post['difficulte'],
                        cout=post['cout'],
                        photo=photo,
                        temps_preparation=post['temps_preparation'],
                        temps_cuisson=post['temps_cuisson'],
                        temps_repos=post['temps_repos'],
                        ingredients=post['ingredients'],
                        etapes=post['etapes'],
                        nb_notes=0,
                        note=0.0,
                        utilisateur=request.user,
                        nb_commentaires=0,
                        date_ajout=datetime.now()
                    )
                    recette.save()

                    return redirect('recette:index')
                else:
                    erreur = 'Une recette de ce titre existe déjà !'
            else:
                erreur = 'Tous les champs sont obligatoires !'
        else:
            erreur = 'Erreur dans le formulaire'
    else:
        formulaire = RecetteForm()

    return render(request, 'ajouter.html', {
        'formulaire': formulaire,
        'erreur': erreur
    })


@login_required(login_url='utilisateurs:connexion')
def modifier(request, slug):
    recette = get_object_or_404(Recette, slug=slug)

    if request.user != recette.utilisateur:
        raise PermissionDenied

    formulaire = RecetteForm(request.POST or None, instance=recette)

    if request.POST and formulaire.is_valid():
        recette.date_modification = datetime.now()
        formulaire.save()
        return redirect('recette:voir', slug=recette.slug)

    return render(request, 'modifier.html', {
        'recette': recette,
        'formulaire': formulaire
    })


@login_required(login_url='utilisateurs:connexion')
def supprimer(request, slug):
    recette = get_object_or_404(Recette, slug=slug)

    # Si on est l'auteur de la recette
    if request.user != recette.utilisateur:
        raise PermissionDenied

    recette.delete()

    return redirect('recette:index')


@login_required(login_url='utilisateurs:connexion')
def noter(request, slug):
    recette = get_object_or_404(Recette, slug=slug)

    # Un vote par utilisateur possible
    if not Note.objects.filter(recette=recette, utilisateur=request.user).exists():
        if request.method == "POST":
            post = request.POST
            note = Note(
                note=post['note'],
                utilisateur=request.user,
                recette=recette
            )
            note.save()

            # Calcul de la note moyenne
            notes = Note.objects.filter(recette_id=recette.id)
            recette.note = (recette.note + float(note.note)) / float(notes.count())
            recette.nb_notes += 1
            recette.save()

    return redirect('recette:voir', slug=recette.slug)


@login_required(login_url='utilisateurs:connexion')
def commenter(request, slug):
    recette = get_object_or_404(Recette, slug=slug)

    if request.method == "POST":
        post = request.POST
        commentaire = Commentaire(
            utilisateur=request.user,
            recette=recette,
            commentaire=post['commentaire'],
            date=datetime.now()
        )
        commentaire.save()

        # Mise à jour du nb de commentaires pour la recette
        recette.nb_commentaires += 1
        recette.save()

    return redirect('recette:voir', slug=recette.slug)


# TODO : adapter le formulaire pour trier aussi selon le type/difficulte
def rechercher(request):
    formulaire_tri = TriRechercheForm()

    if request.method == "GET":
        get = request.GET
        if get and 'recette_rechercher' in get and 'ordre' in get:
            critere = request.GET.get('recette_rechercher')
            ordre = get['ordre']
            if 'tri' in get and 'choix' in get:
                tri = get['tri']
                choix = get['choix']
                if tri == "type":
                    recettes = Recette.objects.filter((Q(titre__contains=critere) | Q(ingredients__contains=critere)), type=choix)
                elif tri == "difficulte":
                    recettes = Recette.objects.filter((Q(titre__contains=critere) | Q(ingredients__contains=critere)), difficulte=choix)
            else:
                recettes = Recette.objects.filter(Q(titre__contains=critere) | Q(ingredients__contains=critere))\
                    .order_by(ordre)

            return render(request, 'rechercher.html', {
                'recettes': recettes,
                'formulaire_tri': formulaire_tri,
                'critere': critere,
                'nb_recettes': recettes.count()
            })
        else:
            raise Http404("Erreur dans le formulaire de recherche")
    else:
        raise Http404("Erreur dans le formulaire de recherche")


def aleatoire(request):
    recette_trouve = False
    derniere_recette = Recette.objects.latest('id')
    max_id = int(derniere_recette.id)

    nombre_aleatoire = randint(1, max_id)
    while not recette_trouve:
        if Recette.objects.filter(pk=nombre_aleatoire).count() > 0:
            recette = Recette.objects.get(pk=nombre_aleatoire)
            recette_trouve = True

    all_commentaires = Commentaire.objects.filter(recette=recette)
    a_vote = False
    a_ajouter_au_carnet = False

    commentaires = paginer(request.GET.get('page'), all_commentaires, NB_COMMENTAIRES_PAR_PAGE)

    # Formulaires
    formulaire_note = NoteForm()
    formulaire_commentaire = CommentaireForm()

    # Savoir si l'utilisateur a déjà voté ou non
    if request.user.is_authenticated and Note.objects.filter(recette=recette, utilisateur=request.user).exists():
        a_vote = True

    # Si l'utilisateur a déjà ajouté cette recette à son carnet ou non
    if request.user.is_authenticated and Fiche.objects.filter(utilisateur=request.user, recette=recette).exists():
        a_ajouter_au_carnet = True

    return render(request, 'voir.html', {
        'recette': recette,
        'commentaires': commentaires,
        'formulaire_note': formulaire_note,
        'formulaire_commentaire': formulaire_commentaire,
        'a_vote': a_vote,
        'a_ajouter_au_carnet': a_ajouter_au_carnet
    })


def recette_est_correcte(post, photo):
    if (
        'titre' in post and
        'type' in post and
        'difficulte' in post and
        'cout' in post and
        photo != '' and
        'temps_preparation' in post and
        'temps_cuisson' in post and
        'temps_repos' in post and
        'ingredients' in post and
        'etapes' in post
    ):
        return True
    else:
        return False


def paginer(page, objects, nb):
    paginator = Paginator(objects, nb)

    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)

    return objects