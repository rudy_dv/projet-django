from django.conf.urls import url
from . import views

app_name = 'recette'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^voir/(?P<slug>[\w-]+)/$', views.voir, name='voir'),
    url(r'^voir/(?P<slug>[\w-]+)/noter$', views.noter, name='noter'),
    url(r'^voir/(?P<slug>[\w-]+)/commenter$', views.commenter, name='commenter'),
    url(r'^voir/(?P<slug>[\w-]+)/modifier$', views.modifier, name='modifier'),
    url(r'^voir/(?P<slug>[\w-]+)/supprimer$', views.supprimer, name='supprimer'),
    url(r'^rechercher', views.rechercher, name='rechercher'),
    url(r'^ajouter', views.ajouter, name='ajouter'),
    url(r'^aleatoire', views.aleatoire, name='aleatoire')
]
