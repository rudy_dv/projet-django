from django import forms
from recette.models import Recette


# Constantes
NOTE_MIN = 0
NOTE_MAX = 5


class RecetteForm(forms.ModelForm):
    class Meta:
        model = Recette
        exclude = ['nb_notes', 'note', 'slug', 'utilisateur', 'nb_commentaires']


class NoteForm(forms.Form):
    note = forms.IntegerField(min_value=NOTE_MIN, max_value=NOTE_MAX)


class CommentaireForm(forms.Form):
    commentaire = forms.CharField(widget=forms.Textarea)


class TriRechercheForm(forms.Form):
    ORDRES = (
        ('titre', 'Titre croissant'),
        ('-titre', 'Titre décroissant'),
        ('note', 'Note croissante'),
        ('-note', 'Note décroissante'),
        ('difficulte', 'Difficulté croissante'),
        ('-difficulte', 'Difficulté décroissante'),
        ('temps_preparation', 'Temps de préparation croissant'),
        ('-temps_preparation', 'Temps de préparation décroissant')
    )

    ordre = forms.ChoiceField(choices=ORDRES)