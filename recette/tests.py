from django.test import TestCase

from recette.models import Recette


class RecetteTestCase(TestCase):
    def setUp(self):
        Recette.objects.create(titre="lasagne", type=1, difficulte=2, cout=23, temps_preparation=10, temps_cuisson=15,
                               temps_repos=3, ingredients="pate, viande, sauce tomate", etapes= "faire cuire la viande",
                               nb_notes=6, note=5, nb_commentaires=26)

    def test_insert_recette(self):
        lasagne = Recette.objects.get(titre="lasagne")
        lasagne.save()
        self.assertEqual(lasagne.titre, "lasagne")
        self.assertEqual(lasagne.type, 1)
        self.assertEqual(lasagne.difficulte, 2)
        self.assertEqual(lasagne.cout, 23)
        self.assertEqual(lasagne.temps_preparation, 10)
        self.assertEqual(lasagne.temps_cuisson, 15)
        self.assertEqual(lasagne.temps_repos, 3)
        self.assertEqual(lasagne.ingredients, "pate, viande, sauce tomate")
        self.assertEqual(lasagne.etapes, "faire cuire la viande")
        self.assertEqual(lasagne.nb_notes, 6)
        self.assertEqual(lasagne.note, 5)
        self.assertEqual(lasagne.nb_commentaires, 26)

    def test_update_recette(self):
        lasagne_update = Recette.objects.get(titre="lasagne")
        lasagne_update.temps_preparation = 12
        lasagne_update.save()
        self.assertEqual(lasagne_update.titre, "lasagne")
        self.assertEqual(lasagne_update.type, 1)
        self.assertEqual(lasagne_update.difficulte, 2)
        self.assertEqual(lasagne_update.cout, 23)
        self.assertEqual(lasagne_update.temps_preparation, 12)
        self.assertEqual(lasagne_update.temps_cuisson, 15)
        self.assertEqual(lasagne_update.temps_repos, 3)
        self.assertEqual(lasagne_update.ingredients, "pate, viande, sauce tomate")
        self.assertEqual(lasagne_update.etapes, "faire cuire la viande")
        self.assertEqual(lasagne_update.nb_notes, 6)
        self.assertEqual(lasagne_update.note, 5)
        self.assertEqual(lasagne_update.nb_commentaires, 26)