from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
import os
from uuid import uuid4


def path_and_rename(instance, filename):
    upload_to = 'static/images/recette/'
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)
    return os.path.join(upload_to, filename)


class Recette(models.Model):
    TYPES = (
        (0, 'Entrée'),
        (1, 'Plat'),
        (2, 'Dessert')
    )

    DIFFICULTES = (
        (0, 'Facile'),
        (1, 'Moyen'),
        (2, 'Difficile')
    )

    titre = models.CharField(max_length=100)
    type = models.SmallIntegerField(choices=TYPES, default=0)
    difficulte = models.SmallIntegerField(choices=DIFFICULTES, default=0)
    cout = models.FloatField()
    photo = models.ImageField(upload_to=path_and_rename, default='static/images/recette/defaut.png', null=True)
    temps_preparation = models.IntegerField()
    temps_cuisson = models.IntegerField()
    temps_repos = models.IntegerField(null=True, blank=True, default=0)
    ingredients = models.TextField()
    etapes = models.TextField()
    nb_notes = models.IntegerField(default=0)
    note = models.FloatField()
    slug = models.SlugField(unique=True)
    utilisateur = models.ForeignKey(User, related_name='recette_user', null=True)
    nb_commentaires = models.IntegerField(default=0)
    date_ajout = models.DateTimeField(null=True, blank=True)
    date_modification = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.titre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titre)

        super(Recette, self).save(*args, **kwargs)


class Note(models.Model):
    NOTES = (
        (0, 0),
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5)
    )

    note = models.SmallIntegerField(choices=NOTES, default=5)
    utilisateur = models.ForeignKey(User, related_name='note_user')
    recette = models.ForeignKey(Recette, related_name='note_recette')

    def __str__(self):
        return self.note.__str__()


class Commentaire(models.Model):
    utilisateur = models.ForeignKey(User, related_name='commentaire_user')
    recette = models.ForeignKey(Recette, related_name='commentaire_recette')
    commentaire = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return self.commentaire