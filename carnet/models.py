from django.contrib.auth.models import User
from django.db import models

from recette.models import Recette


class Fiche(models.Model):
    utilisateur = models.ForeignKey(User, related_name='fiche_user')
    recette = models.ForeignKey(Recette, related_name='fiche_recette')
    remarque = models.TextField(null=True, blank=True)
