from django.conf.urls import url
from . import views

app_name = 'carnet'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'ajouter-fiche/(?P<slug>[\w-]+)/$', views.ajouter_fiche, name='ajouter_fiche'),
    url(r'modifier-fiche/(?P<fiche_id>\d+)/$', views.modifier_fiche, name='modifier_fiche'),
    url(r'supprimer-fiche/(?P<fiche_id>\d+)/$', views.supprimer_fiche, name='supprimer_fiche')
]
