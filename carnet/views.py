from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404, redirect

from carnet.forms import FicheForm
from carnet.models import Fiche
from recette.models import Recette


NB_FICHES_PAR_PAGE = 3


@login_required(login_url='utilisateurs:connexion')
def index(request):
    fiches = Fiche.objects.all()
    fiches = paginer(request.GET.get('page'), fiches, NB_FICHES_PAR_PAGE)

    return render(request, 'index.html', {
        'fiches': fiches
    })


@login_required(login_url='utilisateurs:connexion')
def ajouter_fiche(request, slug):
    recette = get_object_or_404(Recette, slug=slug)

    if request.method == "POST":
        formulaire = FicheForm(request.POST)
        if formulaire.is_valid():
            post = request.POST

            if post and 'remarque' in post:
                fiche = Fiche(utilisateur=request.user, recette=recette, remarque=post['remarque'])
            else:
                fiche = Fiche(utilisateur=request.user, recette=recette, remarque='')

            fiche.save()
            return redirect('carnet:index')
    else:
        formulaire = FicheForm()

    return render(request, 'fiche/ajouter.html', {
        'formulaire': formulaire,
        'recette': recette
    })

@login_required(login_url='utilisateurs:connexion')
def modifier_fiche(request, fiche_id):
    fiche = get_object_or_404(Fiche, id=fiche_id)

    if request.user != fiche.utilisateur:
        raise PermissionDenied

    formulaire = FicheForm(request.POST or None, instance=fiche)

    if request.POST and formulaire.is_valid():
        formulaire.save()
        return redirect('carnet:index')

    return render(request, 'fiche/modifier.html', {
        'fiche': fiche,
        'formulaire': formulaire
    })

@login_required(login_url='utilisateurs:connexion')
def supprimer_fiche(request, fiche_id):
    fiche = get_object_or_404(Fiche, id=int(fiche_id))

    # Si on est l'utilisateur de cette fiche
    if request.user != fiche.utilisateur:
        raise PermissionDenied

    fiche.delete()

    return redirect('carnet:index')


def paginer(page, objects, nb):
    paginator = Paginator(objects, nb)

    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)

    return objects