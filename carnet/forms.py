from django import forms

from carnet.models import Fiche


class FicheForm(forms.ModelForm):
    class Meta:
        model = Fiche
        exclude = ['utilisateur', 'recette']