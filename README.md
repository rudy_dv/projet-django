Recette Inshape
===============
Pour installer correctement le projet, il faut disposer des requirements précisés dans le requirements.txt. A savoir :
```bash
pip install django-widget-tweaks
pip install Pillow
```

A la suite de cela, vous pouvez intégrer un jeu de données existant via cette commande :
```bash
python manage.py loaddata db.json
```

Plusieurs utilisateurs sont déjà existants, les voici :

| Nom utilisateur | Mot de passe | 
| ----------------|--------------|
| Rudy         | rudy        | 
| Antoine        | antoine      |
| Adel        | adel      |
| Gautier        | gautier      |