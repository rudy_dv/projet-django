from django.shortcuts import render, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.contrib.auth.models import User
from utilisateurs.forms import ConnexionForm, InscriptionForm


def connexion(request):
    erreur = ''
    if request.method == "POST":
        formulaire = ConnexionForm(request.POST)
        if formulaire.is_valid():
            utilisateur = authenticate(username=request.POST['nom'], password=request.POST['password'])
            if utilisateur is not None:
                login(request, utilisateur)

                if request.GET.get('next'):
                    return redirect(request.GET.get('next'))
                else:
                    return redirect('recette:index')
            else:
                erreur = 'Les informations sont incorrectes !'
    else:
        formulaire = ConnexionForm()

    return render(request, 'connexion.html', {
        'formulaire': formulaire,
        'erreur': erreur
    })


def inscription(request):
    erreur = ''
    if request.POST:
        formulaire = InscriptionForm()
        post = request.POST
        if not utilisateur_existant(post['nom']):
            if post['password'] == post['passwordConfirmation']:
                user = creer_utilisateur(username=post['nom'], email=post['email'], password=post['password'])
                return redirect('recette:index')
            else:
                erreur = 'Les mots de passe doivent être identique !'
        else:
            erreur = 'Ce nom est déjà utilisé !'
    else:
        formulaire = InscriptionForm()

    return render(request, 'inscription.html', {
        'formulaire': formulaire,
        'erreur': erreur
    })


def deconnexion(request):
    logout(request)
    return redirect('recette:index')


def utilisateur_existant(username):
    nb_utilisateurs = User.objects.filter(username=username).count()
    if nb_utilisateurs == 0:
        return False
    return True


def creer_utilisateur(username, email, password):
    utilisateur = User(username=username, email=email)
    utilisateur.set_password(password)
    utilisateur.save()
    return utilisateur