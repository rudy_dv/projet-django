from django import forms


class ConnexionForm(forms.Form):
    nom = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class InscriptionForm(forms.Form):
    nom = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    passwordConfirmation = forms.CharField(widget=forms.PasswordInput())