from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^recette/', include('recette.urls')),
    url(r'^carnet/', include('carnet.urls')),
    url(r'^utilisateurs/', include('utilisateurs.urls')),
    url(r'^admin/', admin.site.urls),
]
